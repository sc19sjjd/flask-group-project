# -*- coding: utf-8 -*-

from __future__ import print_function
from sqlalchemy.sql.expression import column
from app import app, db
from flask import Flask, render_template, flash, request, jsonify, session, make_response, redirect
from flask_login import LoginManager, login_required, login_user, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
import requests
import stripe
from flask_wtf import FlaskForm
from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length
from sqlalchemy import and_
from ast import literal_eval
from datetime import datetime
from .models import *
from .forms import *
import pdfkit
import sys
import base64
from mailjet_rest import Client
import math


stripe.api_key = 'sk_test_51IUAZ0K1PTp3CGw0lHkTmQnFcUuyKXLqPE6aIojQ9DxMpZDnfvrk3gk1LFeR0mDsK2oyp86sNz3AQaGeDjaERuVa00NqCyu61e'

login_manager = LoginManager()
#redirects to login page for routes with @login_required
login_manager.login_view = '/login'
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

@app.before_request
def init():
	if 'email' not in session:
		session['email'] = None
	if 'basket' not in session:
		session['basket'] = []




@app.route('/chooseSeats/<screening_id>', methods=['GET'])
def choose_seats(screening_id):
	screening = Screening.query.filter_by(id = screening_id).first()
	screen = screening.screen
	try:
		seat_map = list(literal_eval(screening.seat_map))
	except:
		seat_map = [ [-1,-1] for i in range(0, screen.rows * screen.columns) ]

	reserved = []
	prices = [ calc_price(screening.price, i) for i in range(0, 3) ]

	try:
		g_seats = list(literal_eval(screen.seat_gaps))
	except:
		g_seats = []
		g_seats.append(int(screen.seat_gaps))

	try:
		g_columns = list(literal_eval(screen.column_gaps))
	except:
		g_columns = []
		g_columns.append(int(screen.column_gaps))

	try:
		g_rows = list(literal_eval(screen.row_gaps))
	except:
		g_rows = []
		g_rows.append(int(screen.row_gaps))

	for ind, seat in enumerate(seat_map):
		reserved.append(seat[0])
		if seat[0] != -1 and ind not in g_seats:
			if screen.columns == g_columns or str(ind % screen.columns) in g_columns:
				continue
			elif ind / screen.columns == g_rows or str(ind / screen.columns) in g_columns:
				continue

	return render_template('chooseSeats.html', screening=screening, screen=screen, g_rows=g_rows, g_columns=g_columns,\
							g_seats=g_seats, prices=prices, reserved=reserved, basket=session['basket'])


@app.route('/chooseSeats/<screening_id>/book', methods=['POST'])
def choose_seats_post(screening_id):
	data = request.get_json()
	print(data)
	basket = session['basket']
	try:
		for i in data['q']['regular']:
			basket.append([int(screening_id), 0, i, 1])
		for i in data['q']['child']:
			basket.append([int(screening_id), 1, i, 1])
		for i in data['q']['senior']:
			basket.append([int(screening_id), 2, i, 1])
	except:
		pass
	session['basket'] = basket
	return account()


# Update basket quantities
@app.route('/update', methods=['POST'])
def update():
	data = request.get_json()
	print(data)
	basket = session['basket']
	n = 0
	try:
		for i in data['u']:
			if int(i['value']) != basket[n][2]:
				basket[n][2] = int(i['value'])
			if int(i['value']) == 0:
				basket.pop(n)
				continue
			n += 1
	except:
		pass
	session.pop('basket', None)
	session['basket'] = basket
	return account()

@app.route('/', methods=['GET', 'POST'])
def film():
	#gives a number value for the age rating
	switcher = {
		'U': 1,
		'PG': 2,
		'12A': 3,
		'15': 4,
		'18': 5
	}

	filtersForm = FiltersForm()
	searchForm = SearchForm()
	filmsInfo = []
	films = []

	#set up for the search term
	if not searchForm.search.data:
		searchForm.search.data = ""
	search = "%{}%".format(searchForm.search.data)

	#if search is used or filters applied
	if searchForm.validate_on_submit() or filtersForm.validate_on_submit():
		for film in Film.query.filter(Film.title.like(search)).all():
			#get each films data
			filmData = get_movie_data(film.imdb_id)

			#get the rating value
			rating = switcher.get(filmData.age_rating, 0)
			#apply filters
			if not filtersForm.ageRating.data or rating <= int(filtersForm.ageRating.data):
				if filtersForm.genre.data == 'Any' or not filtersForm.genre.data:
					filmsInfo.append(filmData)
					films.append(film)
				elif filtersForm.genre.data in filmData.genre:
					filmsInfo.append(filmData)
					films.append(film)

		return render_template('films.html', films=films, filmsInfo=filmsInfo, searchForm=searchForm, filtersForm=filtersForm)

	#if no search or filter used
	for film in Film.query.all():
		filmData = get_movie_data(film.imdb_id)
		filmsInfo.append(filmData)
		films.append(film)

	return render_template('films.html', films = films, filmsInfo=filmsInfo, searchForm=searchForm, filtersForm=filtersForm)

# Gets all screenings of a film
@app.route('/screenings/<id>', methods=['GET'])
def screenings(id):
	screenings = Screening.query.filter(Screening.film_id == id, and_(Screening.date_time >= datetime.now())).order_by(Screening.date_time.asc())
	filmData = get_movie_data(Film.query.filter(Film.id == id).first().imdb_id)

	return render_template('screenings.html', screenings=screenings, film = filmData)

# Gets screenings of a film between two dates, sDate and fDate
@app.route('/screenings/<id>,<sDate>,<fDate>', methods=['GET'])
def date_screenings(id, sDate, fDate):
	screenings = Screening.query.filter(and_(Screening.film_id == id, and_(Screening.date_time >= sDate, Screening.date_time <= fDate)))
	return render_template('screenings.html', screenings=screenings)


# Account page with basket and completed orders w/ tickets
@app.route('/account')
@login_required
def account():
	films = Film.query.all()
	screenings = Screening.query.all()
	#user = User.query.filter_by(email=session['email']).first()
	orders = Order.query.filter_by(user_id=current_user.id).all()
	tickets = []
	basket = session.get('basket')

	for order in orders:
		tickets.append(Ticket.query.filter_by(order_id=order.id).all())

	return render_template('account.html', basket=basket, films=films, seat_name=seat_name,\
		orders=orders, email=current_user.email, screenings=screenings, tickets=tickets)


@app.route('/logout', methods=['GET'])
def logout():
	flash('Logged out')
	logout_user()
	return film()


@app.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()

	if form.validate_on_submit():
		email = form.email.data
		pword = form.password.data
		try:
			user = User.query.filter_by(email=email).first()
		except:
			flash('Please check your login details and try again.')
			return render_template('signin.html', form=form)

		if not user or not check_password_hash(user.pword, pword):
			flash('Please check your login details and try again.')
			return render_template('signin.html', form=form)

		login_user(user)
		session['email'] = email
		app.logger.info(email + " logged in")

		flash('Logged in successfully')
		return account()

	return render_template('signin.html', form=form)


@app.route('/signup', methods=['GET', 'POST'])
def signup():
	form = SignupForm()

	if form.validate_on_submit():
		name = form.name.data
		email = form.email.data
		pword = form.password.data
		confirm = form.confirm.data

		if pword != confirm:
			flash('Passwords do not match.')
			return render_template('signup.html', form=form)

		user = User.query.filter_by(email=email).first()
		if user:
			flash('Email is already in use.')
			return render_template('signup.html', form=form)

		user = User(email=email, name=name, pword=generate_password_hash(pword, method='sha256'), isAdmin=False)

		db.session.add(user)
		db.session.commit()
		login_user(user)
		app.logger.info(email + " signed up")

		flash('Account created successfully.')
		return account()

	return render_template('signup.html', form=form)


# API Key for movie database
OMDB_API_KEY = "870f845a"


# Object to hold infomation about movie from API
class IMDBFilm():
	def __init__(self, d):
		self.id = d["imdbID"]
		self.title = d["Title"]
		self.release_date = d["Released"]
		self.age_rating = d["Rated"]
		self.runtime = d["Runtime"]
		self.genre = d["Genre"]
		self.director = d["Director"]
		self.writer = d["Writer"]
		self.main_actors = d["Actors"]
		self.plot = d["Plot"]
		self.award_summary = d["Awards"]
		self.posterURL = d["Poster"]
		self.age_rating_url = None


# File private function takes the imdb id of the movie,
# calls the OMDb Api to get the movie data
# and serialises the data into a film class which is returned
# TEST DATA: imdb Id = tt0993846 (Wolf of wall street)
def download_movie_data(id):
	response = requests.get("http://www.omdbapi.com/?apikey=" + OMDB_API_KEY + "&i=" + str(id))

	if response.status_code == 200 and not "Error" in response.json():
		return IMDBFilm(response.json())
	else:
		return None


# Dictionary to hold previously downloaded movie data (movie data cache)
# If not previously downloaded the movie data is downloaded and returned
# If previously downloaded movie data is returned from the cache
cachedMovieData = {}


def get_movie_data(id):
	#convert american ratings to uk ratings
	ageSwitcher = {
		'G': 'U',
		'PG': 'PG',
		'PG-13': '12A',
		'R': '15',
		'NC-17': '18'
	}

	#gets the image url for the corresponding age rating
	urlSwitcher = {
		'U': 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/BBFC_U_2019.svg/140px-BBFC_U_2019.svg.png',
		'PG': 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/BBFC_PG_2019.svg/140px-BBFC_PG_2019.svg.png',
		'12A': 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/BBFC_12A_2019.svg/140px-BBFC_12A_2019.svg.png',
		'15': 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/BBFC_15_2019.svg/140px-BBFC_15_2019.svg.png',
		'18': 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/BBFC_18_2019.svg/140px-BBFC_18_2019.svg.png',
	}

	if id in cachedMovieData:
		return cachedMovieData[id]
	else:
		film = download_movie_data(id)
		if not film:
			return None

		film.age_rating = ageSwitcher.get(film.age_rating, film.age_rating)
		film.age_rating_url = urlSwitcher.get(film.age_rating, 'Invalid rating')
		cachedMovieData[id] = film
		return film


def calc_price(price, ticket_type):
	if ticket_type == 0:    # Regular
		return price

	elif ticket_type == 1:  # Child
		return price * 0.7

	elif ticket_type == 2:  # Senior
		return price * 0.6

	elif ticket_type == 3:  # Disabled
		return price * 0.5


def earnings_graph(start, end, maxpoints):
	#calculate number of days between start and end date
	diff = end - start

	#get the number of points + increment size
	if diff.days > 0:
		inc = diff / diff.days
	npoints = diff.days + 1
	if diff.days > maxpoints - 1:
		inc = diff / (maxpoints - 1.01)
		npoints = maxpoints

	#add the labels + dates for points
	points = []
	labels = []
	points.append(start)
	labels.append(datetime.strftime(start, "%d-%m-%y"))
	for i in range(1, npoints):
		points.append(points[i-1] + inc)
		labels.append(datetime.strftime(points[i], "%d-%m-%y"))

	#add the values for the points
	gmax = 0.0
	values = []
	for point in points:
		value = 0.0
		for order in Order.query.filter(Order.date_time.between(start, point)).all():
			for ticket in order.tickets:
				value += ticket.price
		value /= 100
		values.append(value)
		#update the max value
		if value > gmax:
			gmax = value

	gmax = math.ceil(gmax) + 1

	return labels, values, gmax

#returns bar graph values for tickets sold in past week
def weekly_tickets():
	#get start and end dates for the past week
	oneDay = datetime(2021, 1, 2) - datetime(2021, 1, 1)
	oneWeek = oneDay * 6
	end = datetime.utcnow().date()
	start = end - oneWeek

	labels = []
	values = []
	gmax = 0.0
	totalSold = 0

	for i in range(7):
		date = oneDay * i + start
		labels.append(datetime.strftime(date, "%d-%m-%y"))
		orders = Order.query.filter(Order.date_time.between(date, date + oneDay)).all()
		if len(orders) > 1:
			numTickets = 0
			for order in orders:
				numTickets += len(order.tickets)
			values.append(numTickets)
		else:
			values.append(0)
		#update the max value
		if values[i] > gmax:
			gmax = values[i]
		#update total tickets sold
		totalSold += values[i]

	gmax = math.ceil(gmax) + 1

	return labels, values, gmax, totalSold

#view earnings
@app.route('/earnings', methods=['GET', 'POST'])
@login_required
def earnings():
	if not current_user.isAdmin:
		flask("You don't have permission to view this page")
		return redirect('/')

	earningsForm = BetweenDateForm()
	timeFrameEarnings = 0.00
	totalEarnings = 0.00
	totalVals = earnings_graph(datetime(2021, 1, 1, 0, 0, 0), datetime.utcnow(), 30)

	for ticket in Ticket.query.all():
		totalEarnings += ticket.price
	totalEarnings = totalEarnings / 100

	if earningsForm.validate_on_submit():
		startDate = earningsForm.startDate.data
		endDate = earningsForm.endDate.data

		for order in Order.query.filter(Order.date_time.between(startDate, endDate)).all():
			for ticket in order.tickets:
				timeFrameEarnings += ticket.price
		timeFrameEarnings = timeFrameEarnings / 100

		tfVals = earnings_graph(startDate, endDate, 13)

	#defaults to a weeks earnings, ending today
	if request.method == 'GET':
		oneWeek = datetime(2021, 1, 7) - datetime(2021, 1, 1)
		end = datetime.utcnow().date()
		start = end - oneWeek
		tfVals = earnings_graph(start, end, 10)

		for order in Order.query.filter(Order.date_time.between(start, end)).all():
			for ticket in order.tickets:
				timeFrameEarnings += ticket.price
		timeFrameEarnings = timeFrameEarnings / 100

	#get the values for tickets sold in past week bar graph
	ticketVals = weekly_tickets()

	return render_template('earnings.html', totalEarnings=totalEarnings, ticketsSold=ticketVals[3],
							earningsForm=earningsForm, timeFrameEarnings=timeFrameEarnings,
							totalLabels=totalVals[0], totalValues=totalVals[1], totalMax=totalVals[2],
							ticketLabels=ticketVals[0], ticketValues=ticketVals[1], ticketMax=ticketVals[2],
							tfLabels=tfVals[0], tfValues=tfVals[1], tfMax=tfVals[2])

# Admin panel to add screenings to database
@app.route('/admin', methods=['GET', 'POST'])
@login_required
def admin():
	if not current_user.isAdmin:
		flask("You don't have permission to view this page")
		return redirect('/')

	addFilmForm = AddFilmForm()
	form = AddScreeningForm()
	films = Film.query.all()

	#add films to the database
	if addFilmForm.validate_on_submit():
		imdbID = addFilmForm.imdb_id.data
		if Film.query.filter_by(imdb_id=imdbID).first():
			flash('That film has already been added')
			return redirect('/admin')

		mData = get_movie_data(addFilmForm.imdb_id.data)

		if not mData:
			flash('Could not get a film with that id')
			return redirect('/admin')

		newFilm = Film(title=mData.title, imdb_id=mData.id)
		db.session.add(newFilm)
		db.session.commit()

		return redirect('/admin')


	#total and average revenue for each film as lists
	totalRevenues = []
	avrgRevenues = []

	count = 0
	for film in films:
		totalRevenues.append(0.0)
		avrgRevenues.append(0.0)

		#get all the tickets for the film and then add it to the total revenue list
		tickets = db.session.query(Ticket).join(Screening).filter(Screening.film_id == film.id).all()
		for ticket in tickets:
			totalRevenues[count] += ticket.price / 100
		#get all screenings of film to calculate average reveunue per screening
		screenings = db.session.query(Screening).filter(Screening.film_id == film.id).all()
		numScreenings = len(screenings)
		if numScreenings > 0:
			avrgRevenues[count] = totalRevenues[count] / numScreenings

		count += 1

	return render_template('admin.html', addFilmForm=addFilmForm, films=films,
							totalRevenues=totalRevenues, avrgRevenues=avrgRevenues)

@app.route('/adminScreenings/<id>', methods=['GET', 'POST'])
@login_required
def adminScreenings(id):
	if not current_user.isAdmin:
		flask("You don't have permission to view this page")
		return redirect('/')

	film = Film.query.filter_by(id=id).first()
	screenings = Screening.query.filter_by(film_id=id).all()
	form = AddScreeningForm()

	if form.validate_on_submit():
		date_time = form.date_time.data
		screen_id = form.screen_id.data
		price = float(form.price.data) * 100

		screen = Screen.query.filter_by(id=screen_id).first()
		#room_size = screen.room_size

		'''seat_map = []
		w = room_size.split("x")[0] - 1
		h = room_size.split("x")[1] - 1
		y = 0
		while y <= h:
			x = 0
			row = []
			while x >= w:
				row.append(0) # seat map states: 0 = free, 1 = reserved
				x += 1
			seat_map.append(row)
			y += 1'''

		db.session.add(Screening(date_time=date_time, film=film, screen=screen, price=price))
		db.session.commit()

		return redirect('/adminScreenings/' + str(id))


	return render_template('adminScreenings.html', form=form, film=film, screenings=screenings, calc_price=calc_price)

@app.route('/removeFilm/<id>', methods=['GET'])
@login_required
def removeFilm(id):
	if not current_user.isAdmin:
		flask("You don't have permission to view this page")
		return redirect('/')

	film = Film.query.filter_by(id=id).first()
	db.session.delete(film)
	db.session.commit()

	flash('Removed ' + film.title)

	return admin()

@app.route('/removeScreening/<filmId>/<screeningId>', methods=['GET'])
@login_required
def removeScreening(filmId, screeningId):
	if not current_user.isAdmin:
		flask("You don't have permission to view this page")
		return home()

	screening = Screening.query.filter_by(id=screeningId).first()
	db.session.delete(screening)
	db.session.commit()

	flash('Screening removed')

	return redirect('/adminScreenings/' + str(filmId))

def film_tickets_graph(start, end, maxpoints, filmId):
	#calculate number of days between start and end date
	diff = end - start

	#get the number of points + increment size
	if diff.days > 0:
		inc = diff / diff.days
	npoints = diff.days + 1
	if diff.days > maxpoints - 1:
		inc = diff / (maxpoints - 1.01)
		npoints = maxpoints

	#add the labels + dates for points
	points = []
	labels = []
	points.append(start)
	labels.append(datetime.strftime(start, "%d-%m-%y"))
	for i in range(1, npoints):
		points.append(points[i-1] + inc)
		labels.append(datetime.strftime(points[i], "%d-%m-%y"))

	#add the values for the points
	gmax = 0
	total = 0
	values = []
	for point in points:
		value = 0
		for order in Order.query.filter(Order.date_time.between(start, point)).all():
			for ticket in order.tickets:
				if int(ticket.screening.film.id) == int(filmId):
					value += 1
		values.append(value)
		#update the max value
		if value > gmax:
			gmax = value

	total = values[len(values)-1]
	gmax = math.ceil(gmax) + 1

	return labels, values, gmax, total

def film_weekly_tickets(filmId):
	#get start and end dates for the past week
	oneDay = datetime(2021, 1, 2) - datetime(2021, 1, 1)
	oneWeek = oneDay * 6
	end = datetime.utcnow().date()
	start = end - oneWeek

	labels = []
	values = []
	gmax = 0
	totalSold = 0

	for i in range(7):
		value = 0
		date = oneDay * i + start
		labels.append(datetime.strftime(date, "%d-%m-%y"))
		for order in Order.query.filter(Order.date_time.between(date, date + oneDay)).all():
			for ticket in order.tickets:
				if int(ticket.screening.film.id) == int(filmId):
					value += 1
		values.append(value)
		#update the max value
		if value > gmax:
			gmax = value
		#update total tickets sold
		totalSold += value

	gmax = math.ceil(gmax) + 1

	return labels, values, gmax, totalSold

@app.route('/viewFilmDetails/<filmId>', methods=['GET', 'POST'])
@login_required
def viewFilmDetails(filmId):
	if not current_user.isAdmin:
		flask("You don't have permission to view this page")
		return redirect('/')

	filmTitle = Film.query.filter_by(id=filmId).first().title
	dateForm = BetweenDateForm()
	compareForm = CompareForm()

	if compareForm.compare.data == 'None' or compareForm.compare.data == None:
		compareId = 0
		cmpTitle = 'None'
		cmpTotalVals = [[],[],0,0]
		cmpWeekVals = [[],[],0,0]
		cmpTfVals = [[],[],0,0]
	else:
		compareId = Film.query.filter_by(title=compareForm.compare.data).first().id
		cmpTitle = compareForm.compare.data
		cmpTotalVals = film_tickets_graph(datetime(2021, 3, 1, 0, 0, 0), datetime.utcnow(), 20, compareId)
		cmpWeekVals = film_weekly_tickets(compareId)

	totalVals = film_tickets_graph(datetime(2021, 3, 1, 0, 0, 0), datetime.utcnow(), 20, filmId)
	weekVals = film_weekly_tickets(filmId)

	if dateForm.validate_on_submit():
		start = dateForm.startDate.data
		end = dateForm.endDate.data
		tfVals = film_tickets_graph(start, end, 10, filmId)
		if compareId != 0:
			cmpTfVals = film_tickets_graph(start, end, 10, compareId)

	if request.method == 'GET' or compareForm.validate_on_submit() or compareId != 0:
		oneWeek = datetime(2021, 1, 7) - datetime(2021, 1, 1)
		end = datetime.utcnow().date()
		start = end - oneWeek
		tfVals = film_tickets_graph(start, end, 10, filmId)
		if compareId != 0:
			cmpTfVals = film_tickets_graph(start, end, 10, compareId)

	return render_template('filmTicketDetails.html', compareForm=compareForm, filmInfoForm=dateForm,
							filmTitle=filmTitle, cmpTitle=cmpTitle, totalData=totalVals,
							cmpTotalData=cmpTotalVals, weekData=weekVals, cmpWeekData=cmpWeekVals,
							tfData=tfVals, cmpTfData=cmpTfVals)

@app.route('/create-checkout-session', methods=['POST'])
def create_checkout_session():
	lineitems = []
	for i in session['basket']:
		screening = Screening.query.filter_by(id=i[0]).first()
		lineitem = {
			'price_data': {
				'currency': 'gbp',
				'unit_amount': int(calc_price(screening.price, i[1])),
				'product_data': {
					'name': screening.film.title + " - " + intToTicketType(i[1]),
					'images': ["url_for('static',filename='img/replaceme.png')"],
				},
			},
			'quantity': 1,
		}
		lineitems.append(lineitem)
	metadata = {}
	email = []
	if session.get('email'):
		email = session['email']
	try:
		checkout_session = stripe.checkout.Session.create(
			customer_email=email,
			submit_type='pay',
			billing_address_collection='auto',
			shipping_address_collection={
				'allowed_countries': [],
			},
			payment_method_types=['card'],
			line_items=lineitems,
			metadata=metadata,
			mode='payment',
			success_url='http://localhost:5000/success?session_id={CHECKOUT_SESSION_ID}',
			cancel_url='http://localhost:5000/account',
		)
		return jsonify({'id': checkout_session.id})
	except Exception as e:
		#app.logger.warn(checkout_session.id + " transaction failed")
		app.logger.warn("transaction failed")
		return jsonify(error=str(e)), 403

@app.route('/success')
def success():
	s_id = request.args.get('session_id')
	s = stripe.checkout.Session.retrieve(s_id, expand=['customer', 'line_items', 'payment_intent'])
	# quantity = s.line_items.data[-1].quantity
	email = s.customer.email

	ord = Order.query.order_by(Order.id.desc()).first()
	o_id = 0
	if ord:
		o_id = ord.id + 1

	tic = Ticket.query.order_by(Ticket.id.desc()).first()
	t_id = 0
	if tic:
		t_id = tic.id + 1

	user = User.query.filter_by(email=email).first()
	order = Order(id=o_id, user_id=user.id, date_time=datetime.utcnow())

	for t in session.get('basket'):
		screening = Screening.query.filter_by(id=t[0]).first()
		ticket = Ticket(id=t_id, order_id=order.id, screening_id=t[0], type=t[1],\
						price=calc_price(screening.price, t[1]), seat=t[2])
		try:
			smap = list(literal_eval(screening.seat_map))
		except:
			smap = []
		smap.append((t[2], t_id))
		screening.seat_map = str(smap)
		order.tickets.append(ticket)
		db.session.add(ticket)
		t_id += 1

	db.session.add(order)
	db.session.commit()
	session.pop('basket', None)
	app.logger.info(str(o_id) + " transaction success")

	sendEmailConfirmation(order.id)
	return account()

@app.route('/cash_payment/<screening_id>')
def tickets_manually(screening_id):
	screening = Screening.query.filter_by(id=screening_id).first()
	screen = screening.screen
	try:
		seat_map = list(literal_eval(screening.seat_map))
	except:
		seat_map = [[-1, -1] for i in range(0, screen.rows * screen.columns)]

	reserved = []
	prices = [calc_price(screening.price, i) for i in range(0, 3)]

	try:
		g_seats = list(literal_eval(screen.seat_gaps))
	except:
		g_seats = []
		g_seats.append(int(screen.seat_gaps))

	try:
		g_columns = list(literal_eval(screen.column_gaps))
	except:
		g_columns = []
		g_columns.append(int(screen.column_gaps))

	try:
		g_rows = list(literal_eval(screen.row_gaps))
	except:
		g_rows = []
		g_rows.append(int(screen.row_gaps))

	for ind, seat in enumerate(seat_map):
		reserved.append(seat[0])
		if seat[0] != -1 and ind not in g_seats:
			if screen.columns == g_columns or str(ind % screen.columns) in g_columns:
				continue
			elif ind / screen.columns == g_rows or str(ind / screen.columns) in g_columns:
				continue

	return render_template('adminTickets.html', screening=screening, screen=screen, g_rows=g_rows, g_columns=g_columns, \
						   g_seats=g_seats, prices=prices, reserved=reserved, basket=session['basket'], cashPayment=cashPayment)

@app.route('/cash_payment/<screening_id>/book', methods=['POST'])
def choose_seats_post_cash(screening_id):
	data = request.get_json()
	print(data)
	basket = session['basket']
	try:
		for i in data['q']['regular']:
			basket.append([int(screening_id), 0, i, 1])
		for i in data['q']['child']:
			basket.append([int(screening_id), 1, i, 1])
		for i in data['q']['senior']:
			basket.append([int(screening_id), 2, i, 1])
	except:
		pass
	session['basket'] = basket
	return account()

@app.route('/cash_payment_process/<id>')
def cashPayment(id):
	ord = Order.query.order_by(Order.id.desc()).first()
	o_id = 0
	if ord:
		o_id = ord.id + 1

	tic = Ticket.query.order_by(Ticket.id.desc()).first()
	t_id = 0
	if tic:
		t_id = tic.id + 1

	order = Order(id=o_id, user_id=current_user.id, date_time=datetime.utcnow())

	for t in session.get('basket'):
		screening = Screening.query.filter_by(id=t[0]).first()
		ticket = Ticket(id=t_id, order_id=order.id, screening_id=t[0], type=t[1], \
						price=calc_price(screening.price, t[1]), seat=t[2])
		try:
			smap = list(literal_eval(screening.seat_map))
		except:
			smap = []
		smap.append((t[2], t_id))
		screening.seat_map = str(smap)
		order.tickets.append(ticket)
		db.session.add(ticket)
		t_id += 1

	db.session.add(order)
	db.session.commit()
	session.pop('basket', None)
	app.logger.info(str(o_id) + " transaction success")
	return redirect("/adminScreenings/"+id)

@app.route('/update_price/<id>/<price>')
def update_pricing(id, price):
	screening = Screening.query.filter_by(id=id).first()
	screening.price = int(float(price)*100)
	db.session.commit()
	return redirect("/adminScreenings/" + str(screening.film.id))

@app.route('/pdfTicket/<id>')
def pdfTicket(id):

	pdf = generatePDFTicket(id)
	response = make_response(pdf)
	response.headers['Content-Type'] = 'application/pdf'
	response.headers['Content-Disposition'] = 'inline; filename=ticket.pdf'
	return response

def generatePDFTicket(id):

	ticket = Ticket.query.filter_by(id=id).first()

	rendered = render_template('pdfTicket.html',
							   Title = ticket.screening.film.title,
							   Screen = ticket.screening.screen_id,
							   DateTime = ticket.screening.date_time,
							   order_id = ticket.order_id,
							   Ticket_type = intToTicketType(ticket.type),
							   Ticket_id = ticket.id,
							   Seat_Number=seat_name(ticket.seat, ticket.screening_id),
							   Price = int(ticket.price/100))

	pdf = pdfkit.from_string(rendered, False)

	return pdf

def generateEmailConfirmationHTML(orderId):
	tickets = Ticket.query.filter_by(order_id=orderId).all()
	print("Tickets", file=sys.stdout)

	print(render_template('email.html', tickets = tickets), file=sys.stdout)

	return render_template('email.html', tickets = tickets)

def sendEmailConfirmation(orderId):
	html = generateEmailConfirmationHTML(orderId)
	order = Order.query.filter_by(id=orderId).first()

	api_key = 'f1e40ff5953fe85381afba58b1a3d0a7'
	api_secret = '7dbe6d402b5d1d89475247f2918ac1dd'
	mailjet = Client(auth=(api_key, api_secret))
	data = {
		'FromEmail': 'sc19gbd@leeds.ac.uk',
		'FromName': 'Hyde Park Picture House',
		'Subject': 'Your tickets are waiting for you',
		'Html-part': html,
		'Recipients': [{'Email': order.user.email}],
	}
	result = mailjet.send.create(data=data)
	print(result.status_code, file=sys.stdout)
	print(result, file=sys.stdout)

	return "hello"

#Function converts integer to word ticket type
def intToTicketType(x):
	if (x == 0):
		return "Adult"
	elif (x==1):
		return "Child"
	elif x == 2:
		return "Senior"
	else:
		return "Unknown Ticket Type"


def seat_name(seat, screening_id):

	screening = Screening.query.filter_by(id = screening_id).first()
	screen = screening.screen
	try:
		g_seats = list(literal_eval(screen.seat_gaps))
	except:
		g_seats = []
		g_seats.append(int(screen.seat_gaps))
	try:
		g_columns = list(literal_eval(screen.column_gaps))
	except:
		g_columns = []
		g_columns.append(int(screen.column_gaps))
	try:
		g_rows = list(literal_eval(screen.row_gaps))
	except:
		g_rows = []
		g_rows.append(int(screen.row_gaps))

	alphabet = list(map(chr, range(ord('A'), ord('Z') +1 )))
	r = 0
	for ro in g_rows:
		if ro < int(seat / (screen.columns)):
			r -= 1
	row = alphabet[int(seat / (screen.columns)) + r]
	c = 1
	for col in g_columns:
		if col < seat % (screen.columns):
			c -= 1
	column = seat % (screen.columns) + c

	return(row + str(column))
